package com.polytech.app;

/**
  * <p>Display a Hello World message to Polytech and implement a method for testing</p>
  * @author Arsène Fougerouse
  * @version 1.0
  * @since 1.0
  *
  */
public class HelloWorld {
  public static void main(final String[] args) {
    System.out.println("Hello World Polytech");
  }

  /**
    * The double of an integer
    * @return x * 2
    * @param x An integer
    */
  public int doubleX(int x) {
    return x * 2;
  }

}
